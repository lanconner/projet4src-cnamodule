import subprocess
import sys
import httplib, urllib

proc = subprocess.Popen(sys.argv[1].split(' '),stdout=subprocess.PIPE)

while True:
	message = proc.stdout.readline().split("|")
	print message
	if len(message) < 2:
		continue
	params = urllib.urlencode({'from': message[1], 'type': message[0], 'value': message[2]})
	headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
	
	conn = httplib.HTTPConnection("127.0.0.1:8000")
	conn.request("POST", "/setlog", params, headers)
	response = conn.getresponse()
	conn.close()
