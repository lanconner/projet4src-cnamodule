#include <iostream>
#include <string>

#include <alsa/asoundlib.h>//inclusion de la bibotheque ALSA
#include <string.h>
#include <math.h>
#include <arpa/inet.h>

#include <errno.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>

#define LOG_ERROR 'E'
#define LOG_INFO 'I'
#define LOG_WARNING 'W'

char *device="default";  //playback device
int remote_log = 0; 
//#define SERVER_ADDR "10.132.9.147" 
#define DATA_LEN 8192

snd_pcm_t *handle;
int err;
int sockfd,client_sock;
int not_broken;

void print_log(std::string msg,char type)
{
	if(remote_log)
		std::cout << type << "|module streaming|" << msg << std::endl;
	else
		std::cout << msg << std::endl;
}

void signal_pipe_broken(int signum)
{
  print_log("Pipe broken - wait for new incomming connection...",LOG_WARNING);
  not_broken = 0;
}

void signal_exit(int signum)
{
  print_log("Exiting...",LOG_ERROR);
  close(client_sock);
  close(sockfd);
  exit(0);
}

//fonction initialisation ALSA
int initAlsa()
{
  if((err=snd_pcm_open(&handle, device,SND_PCM_STREAM_PLAYBACK,0))<0)
    { 
      print_log("Playback open error :" + std::string(snd_strerror(err)),LOG_ERROR);
      return -1;
    }
  return 0;
}

//fonction configuration ALSA
int configAlsa(unsigned int fe)
{
  if((err=snd_pcm_set_params(handle,
			     SND_PCM_FORMAT_FLOAT_LE,// float 32 bits Little Endian, range -1.0 to 1.0
			     SND_PCM_ACCESS_RW_INTERLEAVED,
			     2,
			     fe,
			     1,
			     500000))<0)
    { //0.5sec
      print_log("Playback config error :" + std::string(snd_strerror(err)),LOG_ERROR);
      return -1;
    }
  return 0;
}

//fonction fermeture ALSA
void closeAlsa()
{
  snd_pcm_close(handle);
}

int main(int argc, char *argv[])
{
  int n;
  struct sockaddr_in serv_addr,client_addr;

  char buffer[DATA_LEN]; //1024octets=>256floats ou 1024 char
  char msg[1000];
  unsigned int i = 0;
  struct timeval timeout;
  int port  = 2222;
  unsigned int fe_last = 44100;

  for (i = 1; i < argc; i++) /* Skip argv[0] (program name). */
    {
      if (strcmp(argv[i], "-port") == 0)  /* Process optional arguments. */
        {
	  if (i + 1 <= argc - 1)  /* There are enough arguments in argv. */
	    port = atoi(argv[i+1]);
	  else
            {
	      std::cout << "usage: module_streaming -ip 1.1.1.1 -port 2222 -room kitchen (-local)" << std::endl;
	      exit(0);
            }
        }
      if (strcmp(argv[i], "-rl") == 0)
        {
	  remote_log = 1;
        }	
      if (strcmp(argv[i], "-help") == 0)
        {
	  printf("usage: module -port 2222\n");
	  exit(0);
        }
    }

  signal(SIGINT, signal_exit);
  signal(SIGPIPE, signal_pipe_broken);
  //creation de la socket

  sockfd=socket(AF_INET,SOCK_STREAM,0);
  if(sockfd<0)
    {
      print_log("Can't create socket",LOG_ERROR);
      exit(1);
    }

  //initialisation de la socket
  bzero(&serv_addr, sizeof(serv_addr));
  serv_addr.sin_port = htons(port);
  serv_addr.sin_family=AF_INET;
  serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    
  if(bind(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr))<0)
    {
      print_log("Can't bind socket",LOG_ERROR);
      exit(1);
    }

  //Ecoute du client
  listen(sockfd,5);// 5=nombre max de connections possibles


  socklen_t len_addr=sizeof(client_addr);

  initAlsa();
  configAlsa(fe_last);

  timeout.tv_sec = 0;
  timeout.tv_usec = 10000;
  n = setsockopt(client_sock, SOL_SOCKET, SO_RCVTIMEO,(char*)&timeout,sizeof(timeout)); 
 
  while(1){
    print_log("Wait for incomming connection",LOG_INFO);
    
    //accepter la connection avec le client
    client_sock=accept(sockfd,(struct sockaddr*)&client_addr,&len_addr);
    if(client_sock<0)
      {
        print_log("Can't create socket",LOG_ERROR);
	
	sprintf(msg,"ERROR on accept %d",errno);
	print_log(msg,LOG_ERROR);
	exit(1);
      }
    
    char ip[16];
    
    inet_ntop( AF_INET, &(client_addr.sin_addr.s_addr), ip, 16);
    print_log("Incomming connection from " + std::string(ip) + ":" + std::to_string(client_addr.sin_port),LOG_INFO);
    
    initAlsa();
    configAlsa(44100);
    snd_pcm_sframes_t frames;

    /* set timeout */
    timeout.tv_sec = 0;
    timeout.tv_usec = 100000;
    n = setsockopt(client_sock, SOL_SOCKET, SO_RCVTIMEO,(char*)&timeout,sizeof(timeout)); 
    not_broken = 1;
    while(not_broken)
      {
	//Read the server 
	bzero(buffer,DATA_LEN);
	n=read(client_sock,buffer,DATA_LEN);
	int coeff;
	if(buffer[0]=='C'){
		unsigned int fe = 0;
		fe=(unsigned char)buffer[2]*256+(unsigned char)buffer[3];

		if(fe_last != fe){
			print_log("New samplerate: "+std::to_string(fe),LOG_INFO);
			closeAlsa();
			initAlsa();
			configAlsa(fe);
			fe_last=fe;
		}
		continue;
	}
	
	frames=snd_pcm_writei(handle,(float*)buffer,n/8);//ecriture sur la carte son
	if(frames<0)
	  frames=snd_pcm_recover(handle,frames,0);
	if(frames<0){
	  printf("snd_pcm_writei failed : %s\n", snd_strerror(err));
	  break;
	}
	n=send(client_sock,"need_data",10,MSG_NOSIGNAL);
	if(errno == EPIPE){ // broken pipe <=> connection ended by client
	  print_log("Broken pipe - connection ended by client",LOG_WARNING);
	  break;
	}
      }
    close(client_sock);
  }
  closeAlsa();
  return 0;
}

